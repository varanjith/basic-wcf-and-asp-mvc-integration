﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WCF.WebUI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Result = "";
            return View();
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            MathServiceRef.IMatheService svc = new MathServiceRef.MatheServiceClient();
            MathServiceRef.MyNumbers num = new MathServiceRef.MyNumbers();
            num.Number1 = int.Parse(form["txt_Number1"].ToString());
            num.Number2 = int.Parse(form["txt_Number2"].ToString());
            ViewBag.Result = svc.Add(num).ToString();
            return View();
        }
    }
}